# ics-ans-role-iptables-ruler

Ansible role to install iptables-ruler.

## Role Variables

```yaml
...
iptables_on: true
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-iptables-ruler
```

## License

BSD 2-clause
